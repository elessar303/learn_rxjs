const Task = require('../models/Model')
const { subject } = require('../observables/Logger')
require('../utilities/Utilities')

exports.list_all_tasks = function(req, res) {
  Task.find({}, function(err, task) {
    subject.next('Listing tasks')
    if (err)
      res.send(err)
    res.json(task)
  })
}

exports.create_a_task = function(req, res) {
  var new_task = new Task(req.body)
  new_task.save(function(err, task) {
    subject.next('Creating task')
    if (err)
      res.send(err)
    subject.next(task)
    res.json(task)
  });
};

exports.read_a_task = function(req, res) {
  Task.findById(req.params.taskId, function(err, task) {
    subject.next('Listing task')
    if (err)
      res.send(err)
    res.json(task)
  })
}

exports.update_a_task = function(req, res) {
  Task.findOneAndUpdate({_id: req.params.taskId}, req.body, {new: true}, function(err, task) {
    subject.next('Updating task')
    if (err)
      res.send(err)
    subject.next(task)
    res.json(task)
  })
}

exports.delete_a_task = function(req, res) {
  Task.findOneAndDelete({
    _id: req.params.taskId
  }, function(err, task) {
    subject.next('Deleting task')
    if (err)
      res.send(err)
    subject.next(task)
    res.json(task)
  })
}