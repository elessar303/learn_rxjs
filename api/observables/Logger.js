const { Subject } = require('rxjs')

const subject = new Subject()
module.exports.subject = subject
subject.subscribe(data => console.log(data))