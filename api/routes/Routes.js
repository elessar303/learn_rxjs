module.exports = function(app) {
  var Controller = require('../controllers/Controller')

  // Controller Routes
  app.route('/tasks')
    .get(Controller.list_all_tasks)
    .post(Controller.create_a_task)


  app.route('/tasks/:taskId')
    .get(Controller.read_a_task)
    .put(Controller.update_a_task)
    .delete(Controller.delete_a_task)
}