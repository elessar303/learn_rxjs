const fs = require('fs')
const { subject } = require('../observables/Logger')

subject.subscribe(
    data => fs.appendFile(
        'log.txt', 
        '\n' + data, 
        function (err) {
            if (err) throw err
            console.log('Saved')
        }
    )
)