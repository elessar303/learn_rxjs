var express = require('express'),
  app = express(),
  port = 3000,
  mongoose = require('mongoose'),
  Task = require('./api/models/Model'), //created model loading here
  bodyParser = require('body-parser')
  
// mongoose instance connection url connection
mongoose.Promise = global.Promise
mongoose.connect('mongodb://172.17.0.3/Task', { useNewUrlParser: true })

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

var routes = require('./api/routes/Routes'); //importing route
routes(app) //register the route

app.listen(port)

console.log('Api en el puerto: ' + port)